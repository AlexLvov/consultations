'head': {
    defaults: {
        title: 'Індекс',
        useSocialMetaTags: false
    },
    home: {
        title: 'Головна',
        useSocialMetaTags: false
    },
    about: {
        title: 'Про проект',
        useSocialMetaTags: false
    },
    consultation: {
        title: 'Консультація',
        useSocialMetaTags: false
    },
    consultations: {
        title: 'Консультації',
        useSocialMetaTags: false
    },
    deputies: {
        title: 'Депутати',
        useSocialMetaTags: false
    },
    instructions: {
        title: 'Інструкції',
        useSocialMetaTags: false
    },
    law: {
        title: 'Нормативно-правова база',
        useSocialMetaTags: false
    },
    persona: {
        title: 'Депутат',
        useSocialMetaTags: false
    },
    news: {
        title: 'Новини',
        useSocialMetaTags: false
    },
    post: {
        title: 'Новина',
        useSocialMetaTags: false
    },
    boardComposition: {
        title: 'Склад Ради',
        useSocialMetaTags: false
    },
    rollcall: {
        title: 'Поіменне голосування',
        useSocialMetaTags: false
    }
}
,

__iconsData: {
    
        'acrobat': {
            width: '30px',
            height: '30px'
        },
    
        'add': {
            width: '30px',
            height: '30px'
        },
    
        'ajust': {
            width: '30px',
            height: '30px'
        },
    
        'arrow-big': {
            width: '30px',
            height: '30px'
        },
    
        'arrow-round-bright': {
            width: '30px',
            height: '30px'
        },
    
        'arrow-round': {
            width: '30px',
            height: '29px'
        },
    
        'arrow-small': {
            width: '30px',
            height: '30px'
        },
    
        'attach': {
            width: '30px',
            height: '30px'
        },
    
        'attention-rounded': {
            width: '30px',
            height: '30px'
        },
    
        'backup': {
            width: '30px',
            height: '30px'
        },
    
        'calculator': {
            width: '30px',
            height: '30px'
        },
    
        'calendar': {
            width: '30px',
            height: '30px'
        },
    
        'camera': {
            width: '68px',
            height: '50px'
        },
    
        'cancel': {
            width: '30px',
            height: '30px'
        },
    
        'chat': {
            width: '30px',
            height: '30px'
        },
    
        'close-big': {
            width: '30px',
            height: '30px'
        },
    
        'close-rounded': {
            width: '30px',
            height: '30px'
        },
    
        'close': {
            width: '30px',
            height: '30px'
        },
    
        'close_1': {
            width: '30px',
            height: '30px'
        },
    
        'code': {
            width: '30px',
            height: '30px'
        },
    
        'creative-commons': {
            width: '30px',
            height: '30px'
        },
    
        'done-rounded': {
            width: '30px',
            height: '30px'
        },
    
        'done': {
            width: '30px',
            height: '30px'
        },
    
        'dot': {
            width: '30px',
            height: '30px'
        },
    
        'download': {
            width: '30px',
            height: '30px'
        },
    
        'edit': {
            width: '30px',
            height: '30px'
        },
    
        'expand': {
            width: '30px',
            height: '30px'
        },
    
        'external-link': {
            width: '30px',
            height: '30px'
        },
    
        'facebook': {
            width: '30px',
            height: '30px'
        },
    
        'help': {
            width: '30px',
            height: '30px'
        },
    
        'hide': {
            width: '30px',
            height: '30px'
        },
    
        'hide_1': {
            width: '30px',
            height: '30px'
        },
    
        'keyboard-arrow': {
            width: '30px',
            height: '30px'
        },
    
        'language': {
            width: '30px',
            height: '30px'
        },
    
        'link': {
            width: '30px',
            height: '30px'
        },
    
        'location': {
            width: '30px',
            height: '30px'
        },
    
        'menu': {
            width: '30px',
            height: '30px'
        },
    
        'minimize': {
            width: '30px',
            height: '30px'
        },
    
        'money': {
            width: '30px',
            height: '30px'
        },
    
        'phone': {
            width: '30px',
            height: '30px'
        },
    
        'piece': {
            width: '30px',
            height: '30px'
        },
    
        'play-new': {
            width: '48px',
            height: '48px'
        },
    
        'print': {
            width: '30px',
            height: '30px'
        },
    
        'reload': {
            width: '30px',
            height: '30px'
        },
    
        'reminder': {
            width: '30px',
            height: '30px'
        },
    
        'rss': {
            width: '30px',
            height: '30px'
        },
    
        'search': {
            width: '30px',
            height: '30px'
        },
    
        'settings-big': {
            width: '30px',
            height: '30px'
        },
    
        'settings-small': {
            width: '30px',
            height: '30px'
        },
    
        'shedule': {
            width: '30px',
            height: '30px'
        },
    
        'signal': {
            width: '30px',
            height: '30px'
        },
    
        'sound-off': {
            width: '30px',
            height: '30px'
        },
    
        'sound-on': {
            width: '30px',
            height: '30px'
        },
    
        'sound': {
            width: '30px',
            height: '30px'
        },
    
        'time': {
            width: '30px',
            height: '30px'
        },
    
        'timer': {
            width: '30px',
            height: '30px'
        },
    
        'user-big': {
            width: '30px',
            height: '30px'
        },
    
        'user': {
            width: '30px',
            height: '30px'
        },
    
        'visible': {
            width: '30px',
            height: '30px'
        },
    
        'wait': {
            width: '30px',
            height: '30px'
        },
    
        'warning': {
            width: '30px',
            height: '30px'
        },
    
        'youtube': {
            width: '30px',
            height: '30px'
        },
    
},

__pages: [{
                name: 'about',
                href: 'about.html'
             },{
                name: 'alerts',
                href: 'alerts.html'
             },{
                name: 'compaison',
                href: 'compaison.html'
             },{
                name: 'consultation-edit-full',
                href: 'consultation-edit-full.html'
             },{
                name: 'consultation-edit',
                href: 'consultation-edit.html'
             },{
                name: 'consultation-success',
                href: 'consultation-success.html'
             },{
                name: 'consultation',
                href: 'consultation.html'
             },{
                name: 'consultations',
                href: 'consultations.html'
             },{
                name: 'deputies',
                href: 'deputies.html'
             },{
                name: 'deputy-edit',
                href: 'deputy-edit.html'
             },{
                name: 'deputy',
                href: 'deputy.html'
             },{
                name: 'email',
                href: 'email.html'
             },{
                name: 'email_done',
                href: 'email_done.html'
             },{
                name: 'feedback',
                href: 'feedback.html'
             },{
                name: 'home',
                href: 'home.html'
             },{
                name: 'home_no-login',
                href: 'home_no-login.html'
             },{
                name: 'index',
                href: 'index.html'
             },{
                name: 'instructions',
                href: 'instructions.html'
             },{
                name: 'law',
                href: 'law.html'
             },{
                name: 'login',
                href: 'login.html'
             },{
                name: 'login_not-loged',
                href: 'login_not-loged.html'
             },{
                name: 'news',
                href: 'news.html'
             },{
                name: 'poll-edit',
                href: 'poll-edit.html'
             },{
                name: 'poll-result',
                href: 'poll-result.html'
             },{
                name: 'poll',
                href: 'poll.html'
             },{
                name: 'post',
                href: 'post.html'
             },{
                name: 'registration',
                href: 'registration.html'
             },{
                name: 'ui',
                href: 'ui.html'
             },{
                name: 'user-edit',
                href: 'user-edit.html'
             }]