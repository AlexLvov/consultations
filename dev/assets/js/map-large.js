

var map, infoBubble;
function init() {
  var mapCenter = new google.maps.LatLng(50.4501, 30.5234);
  map = new google.maps.Map(document.getElementById('map-main'), {
    center: mapCenter,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    // How zoomed in you want the map to start at (always required)
    zoom: 11,

    scrollwheel: false,


    // How you would like to style the map.
    // This is where you would paste any style found on Snazzy Maps.
    styles: [{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"lightness":"40"},{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"administrative.province","elementType":"labels","stylers":[{"visibility":"simplified"},{"lightness":"10"}]},{"featureType":"administrative.locality","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"administrative.locality","elementType":"labels","stylers":[{"visibility":"simplified"},{"lightness":"25"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#243c64"}]},{"featureType":"landscape","elementType":"all","stylers":[{"hue":"#ff8800"},{"saturation":"19"},{"lightness":"11"},{"gamma":1}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-1.0989010989011234},{"lightness":11.200000000000017},{"gamma":1}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#576377"},{"lightness":"31"},{"saturation":"-30"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":"30"}]},{"featureType":"road","elementType":"labels.text","stylers":[{"weight":"1"},{"saturation":"0"},{"lightness":"10"},{"gamma":"1"},{"visibility":"on"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#243c64"},{"lightness":"-32"},{"saturation":"43"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"hue":"#ff8800"},{"saturation":-61.8},{"lightness":"81"},{"gamma":1}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"hue":"#ff0300"},{"saturation":-100},{"lightness":51.19999999999999},{"gamma":1}]},{"featureType":"road.local","elementType":"all","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":52},{"gamma":1}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#0080ff"},{"saturation":"-7"},{"lightness":"7"},{"gamma":1}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"},{"lightness":"30"}]}]
  });

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(50.4501, 30.5234)
  });

  var contentString =
    '<div class="map-popover">' +
      '<a class="map-popover__wrap clearfix" href="#">' +
        '<span class="map-popover__img">' +
          '<img class="img-scale" src="assets/media/content/deputy/48x48/1.jpg", alt="foto">' +
        '</span>' +
        '<span class="map-popover__inner">' +
          '<span class="map-popover__name">Кужель Марія Степанівна</span>' +
          '<span class="map-popover__info">Національна комісія, що здійснює державне регулювання у сферах енергетики' +
        '</span>' +
      '</a>' +
      '<button draggable="false" title="Закрити" aria-label="Закрити" type="button" class="gm-ui-hover-effect" style="background: none; display: block; border: 0px; margin: 0px; padding: 0px; position: absolute; cursor: pointer; user-select: none; top: -6px; right: -6px; width: 30px; height: 30px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2224px%22%20height%3D%2224px%22%20viewBox%3D%220%200%2024%2024%22%20fill%3D%22%23000000%22%3E%0A%20%20%20%20%3Cpath%20d%3D%22M19%206.41L17.59%205%2012%2010.59%206.41%205%205%206.41%2010.59%2012%205%2017.59%206.41%2019%2012%2013.41%2017.59%2019%2019%2017.59%2013.41%2012z%22%2F%3E%0A%20%20%20%20%3Cpath%20d%3D%22M0%200h24v24H0z%22%20fill%3D%22none%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="pointer-events: none; display: block; width: 14px; height: 14px; margin: 8px;"></button>' +
    '</div>';


  infoBubble = new InfoBubble({
    maxWidth: 384,
    content: contentString
  });

  infoBubble.open(map, marker);


}
google.maps.event.addDomListener(window, 'load', init);
