/*
| ----------------------------------------------------------------------------------
| TABLE OF CONTENT
| ----------------------------------------------------------------------------------

-Preloader
-Datepicker
-Readmore
-Scale images
-Select customization
-Zoom Images
-Slider
-Select col
-Upload
-Move block on desctop
-Form validation
-Input effects
*/



$(document).ready(function() {

  "use strict";



// PRELOADER //

    var $preloader = $('#page-preloader'),
    $spinner   = $preloader.find('.spinner-loader');
    $spinner.fadeOut();
    $preloader.delay(50).fadeOut('slow');

// DATEPICKER //

  if ($('.input-group.date').length) {
    $('.input-group.date').datepicker({
      language: "uk",
      daysOfWeekHighlighted: "0,6",
      todayHighlight: true
    });
  }



// READMORE //

  if ($('.js-readmore').length) {
    $('.js-readmore').readmore({
      collapsedHeight: 150,
      moreLink: '<a class="b-persona__link" href="#">Смотреть все</a>',
      lessLink: '<a class="b-persona__link" href="#">Свернуть</a>'
    });
  }



// SCALE IMAGES

  if ($('.img-scale').length) {
    $(function () { objectFitImages('.img-scale') });
  }


// SELECT CUSTOMIZATION


if ($('.select_box').length) {
   $(function() {
    var $select = $('.select_box').selectpicker({
      noneResultsText: "нічого не знайдено {0}"
    });

    $(':reset').on('click', function(evt) {
        evt.preventDefault();
        var $form = $(evt.target).closest('form');
        $form[0].reset();
        $form.find('select').selectpicker('render')
    });
  })
}


// ZOOM IMAGES //

  if ($('.js-zoom-gallery').length) {
      $('.js-zoom-gallery').each(function() { // the containers for all your galleries
          $(this).magnificPopup({
              delegate: '.js-zoom-gallery__item', // the selector for gallery item
              type: 'image',
              gallery: {
                enabled:true
              },
        mainClass: 'mfp-with-zoom', // this class is for CSS animation below

        zoom: {
          enabled: true, // By default it's false, so don't forget to enable it

          duration: 300, // duration of the effect, in milliseconds
          easing: 'ease-in-out', // CSS transition easing function

          // The "opener" function should return the element from which popup will be zoomed in
          // and to which popup will be scaled down
          // By defailt it looks for an image tag:
          opener: function(openerElement) {
            // openerElement is the element on which popup was initialized, in this case its <a> tag
            // you don't need to add "opener" option if this code matches your needs, it's defailt one.
            return openerElement.is('img') ? openerElement : openerElement.find('img');
          }
        }
          });
      });
    }


  if ($('.js-zoom-images').length) {
      $('.js-zoom-images').magnificPopup({
        type: 'image',
        mainClass: 'mfp-with-zoom', // this class is for CSS animation below

        zoom: {
          enabled: true, // By default it's false, so don't forget to enable it

          duration: 300, // duration of the effect, in milliseconds
          easing: 'ease-in-out', // CSS transition easing function

          // The "opener" function should return the element from which popup will be zoomed in
          // and to which popup will be scaled down
          // By defailt it looks for an image tag:
          opener: function(openerElement) {
            // openerElement is the element on which popup was initialized, in this case its <a> tag
            // you don't need to add "opener" option if this code matches your needs, it's defailt one.
            return openerElement.is('img') ? openerElement : openerElement.find('img');
          }
        }
      });

    }


  if ($('.popup-youtube, .popup-vimeo, .popup-gmaps').length) {
    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
      disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,

      fixedContentPos: false,
      zoom: {
        enabled: true,
        duration: 300 // don't foget to change the duration also in CSS
      }
    });
  }


// SLIDER //

  if ($('.js-slick').length) {
    $('.js-slick').slick();
  };


// SELECT COL //

  $(".js-select-col").hover(function() {
    var ind = $(this).index();
    $(".js-select-col").parent().each(function() {
      $(this).find(".js-select-col").eq(ind).addClass("hovered");
    })

  }, function() {
    $(".js-select-col").removeClass("hovered");
  });


// Move block on desctop
  if ($(window).width() > 767) {
    $(".js-designer-gift").appendTo($(".b-consult-f_aside"));
  }


// Upload face
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#userFace').attr('src', e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#userInput").change(function(){
      readURL(this);
  });


// UPLOAD

  // if ($(".js-upload").length){
  //   $(".js-upload").dropzone({
  //     url: "../php/index.php",
  //     clickable: ".js-upload-btn",
  //     previewsContainer: ".js-upload-container",
  //     previewTemplate: "<div class='row form-group justify-content-between'><span class='col-auto ui-upload-name' data-dz-name></span><span class='col text-right'><a class='btn-cancel btn btn-default' href='#' data-dz-remove >Cкасувати<svg class='ic' width='30' height='30'><use xlink:href='../svg-symbols.svg#close'></use></svg></a></span></div>"
  //   });
  // }



// Add poll

  $('.js-add-poll').on('click', function() {
    $(this).before('<div class="form-group"><div class="ui-label">Варіант відповіді</div><div class="ui-input"><label class="ui-input__label" for="inputAnswer2"><span class="ui-input__label-content">Введіть варіант відповіді</span></label><input class="ui-input__field form-control" id="inputAnswer2" type="text"><div class="invalid-feedback">Поле не заповненне</div><button class="ui-input__btn-del js-del-group" type="button"><svg class="ic" width="24" height="24"><use xlink:href="../svg-symbols.svg#close"></use></svg>                      </button></div></div>');
  });


// Remove poll
  $('html').on('click', '.js-del-group', function() {
      $(this).parents('.form-group').remove();
  });

});





// FORM VALIDATION

window.addEventListener('load', function() {
  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.getElementsByClassName('needs-validation');
  // Loop over them and prevent submission
  var validation = Array.prototype.filter.call(forms, function(form) {
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  });
}, false);



// INPUT EFFECTS

(function() {
    if (!String.prototype.trim) {
        (function() {
            var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
            String.prototype.trim = function() {
                return this.replace(rtrim, '');
            };
        })();
    }

    [].slice.call( document.querySelectorAll( '.js-input' ) ).forEach( function( inputEl ) {
        if( inputEl.value.trim() !== '' ) {
            classie.add( inputEl.parentNode, 'ui-input_filled' );
        }
        inputEl.addEventListener( 'focus', onInputFocus );
        inputEl.addEventListener( 'blur', onInputBlur );
    } );

    function onInputFocus( ev ) {
        classie.add( ev.target.parentNode, 'ui-input_filled' );
    }

    function onInputBlur( ev ) {
        if( ev.target.value.trim() === '' ) {
            classie.remove( ev.target.parentNode, 'ui-input_filled' );
        }
    }
})();

